const { Front, SubnetParam } = require("../models");
const fController = require("./frontController");

const { Worker } = require('worker_threads');

/** @module botController*/
/**
 * <pre>
 * APIs for handling basic bot activity 
 * API prefix : /bots/
 * Provided APIs : startFront
 *                 stopFront
 *                 getFrontStatus
 * 
 * </pre>
 */
module.exports = {

  /* front running ... */
  /**
   * @export @function API : /bots/startFront
   * 
   * @description 
   * <pre>
   * Main API method to start bot operation. 
   * Actions : 
   *         call followTransaction to follow the transactions made for given wallets.
   *         Set the bot running status as 1.
   *         save to Front Database for given information.
   * </pre>
   * 
   * @param {string} wallet - Wallet address (Public key)
   * @param {string} key - Wallet private KEY(Should be paid attention.) 
   * @param {string} safetyWallet - SafetyWallete to store bought tokens.
   * @param {String} gasSetting - Gas Setting - Block Native or MM Recommended Gas (Default : Native) mm/native
   * @param {Object} subnetParam - Parameter for subnetwork 
   * <pre>
   *  The structure is : 
   *    {
   *      ETH: {
   *        name:"Ethereum Network",    acronym:"ETH",  symbol:"ETH", 
   *        enabled:false, 
   *        nodeUrl:"", scanApikey:"",
   *        amount:0, slippage:0, 
   *        minAmount:0, maxAmount: 0,
   *      },
   *      ...
   *    }
   * </pre>
   * @return  front data (status, wallet, key, safetyWallet, token, subNetParam ... ) 
   * @return  Error message on fail.
   */
  startFront(req, res) {
    const {
      wallet, key,
      safetyWallet,
      gasSetting,
      subnetParams,
    } = req.body;

    // First of all, save the parameters into database.

    // Save Main Bot Status
    const status = "1";
    Front.update(
      {
        status: status,
        wallet: wallet,
        key: key,
        safetyWallet: safetyWallet,
        gasSetting: gasSetting
      },
      {
        where: {
          id: 1,
        },
      }
    ).then((front) =>
      res.status(201).json({
        error: false,
        data: front,
        message: "setting has been updated in the front running",
      })
    ).catch((error) =>
      res.json({
        error: true,
        error: error,
      })
    );

    // initialize subscription object.
    stopBotFlag = false;

    Object.keys(subnetParams).forEach(symbol => {

      const subnetParameter = subnetParams[symbol];
      // Save the subnet parameter.
      SubnetParam.upsert(
        subnetParameter
      ).then((result) => result
      ).catch(error => error
      );

      if(subnetParameter.enabled) {
        fController.followTransaction(
          wallet, key,
          safetyWallet,
          gasSetting,
          subnetParameter,
        ).catch(err => console.log("Front scan mempool error...... for ", symbol));  
      }
    });

      // Invoke new thread for the given subnetwork parameter
    // Call scan Memory Pool for each network.

    // const worker = new Worker('./scanThread.js', {
    //   workerData: { wallet, key, safetyWallet, gasSetting, subnet }
    // });

    // worker.on('message', result=> {
    //   worker.terminate()
    //   resolve(result)
    // })
    // worker.on('error', reject)
    // worker.on('exit', code => {
    //   if (code !== 0) reject(new Error(`Worker stopped with exit code ${code}`))
    // })

  },

  /**
   * @export @function API : /bots/stopFront
   * 
   * @description 
   * <pre>
   * Stop Front action.
   * Actions : 
   *     unsubcribe from the Web3 Front subscription, if there was active one.
   *     Update the Front status to 0, which is NOT RUNNING
   * </pre>
   * 
  * @return  Front data (status, node, wallet, key, token, amount, slippage, gasSetting) 
   * @return  Error message on fail.
   */
  stopFront(req, res) {
    const {
      subnetParam,
    } = req.body;

    stopBotFlag = true;
    // Update the status flag as 0 (STOPPED)
    Front.update(
      {
        status: "0",
      },
      {
        where: {
          id: 1,
        },
      }
    ).then((fdata) =>
      res.status(201).json({
        error: false,
        data: fdata,
        message: "setting has been updated in the front running",
      })
    ).catch((error) =>
      res.json({
        error: true,
        error: error,
      })
    );
  },

  /**
  * @export @function API : /bots/getFrontStatus
  * 
  * @description 
  * <pre>
  * Get the Front status. - Retrieve the Front object from database and return it. <br>
  * If there is no front object(empty), create new one, and return it. In this case, the status is 0,
  * </pre>
  * @return  Front status (1 or 0)
  * @return  Error message on fail.
  */
  getFrontStatus (req, res) {
    console.log(" ========= getFrontStatus called ========= ");
    Front.findAll({
      attribute: "status",
      where: { id: 1 },
    }).then((front) => {
      if (front.length == 0) {
        let item = { id: 1, status: 0, node: "", network: "", wallet: "", key: "", safetyWallet: "", gasSetting: "native" };
        // Create new front with status 0.
        Front.create(item).then((data) => {
          let result = data.dataValues;
          result.subnetParam = Array(0);
          res.status(201).json({
            error: false,
            data: result,
            message: "setting has been created in the Front",
          });
        });
      } else {
        SubnetParam.findAll({
        }).then( (subnetData) => {
          let result = front[0].dataValues;
          result.subnetParam = subnetData.map(r => r.dataValues);
          res.status(201).json({
            error: false,
            data: result,
            message: "setting has been updated in the Front",
          });
        }) ;
      }
    }).catch ((error) =>
      res.json({
        error: true,
        error: error,
      })
    );
  },
};
