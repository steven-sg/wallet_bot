const { ERC20_ABI, BOT_NODE_HANDLERS, MAX_BIGINT } = require("../constant/erc20");
const { FrontDetail, Token, BlackToken } = require("../models");
const ethers = require("ethers");
const chalk = require("chalk");
const app = require("../app.js");
const axios = require('axios');
const { ENTERED } = require("react-transition-group/Transition");


// Send the response to the frontend so let the frontend display the event.
var aWss = null;

let subnetHandler;

/////////////////////////////////////////////////////////////////////////
//                          FOR SOCKET OPERATION                       //
/////////////////////////////////////////////////////////////////////////
/*
const express = require('express');
const http = require ('http');

app = express();
var port = 9081;
const server = http.createServer(app);

server.listen(port, function(){
  console.log('app listening on port: '+port);
});
const sockets = {};
const io = require("socket.io")(server);
var socketInstance;
io.on("connection", (socket) => {
  sockets[sessionId] = socket.id;
  // The sockets object is stored in Express so it can be grabbed in a route
  server.set("sockets", sockets);
  console.log(`Client connected: ${socket.id}`)
  socketInstance = io.to(thisSocketId);
});
// The io instance is set in Express so it can be grabbed in a route
app.set("io", io);
*/

/**
 * @function send Progress information to the client(s).
 * @description
 * <pre>
 *
 * </pre>
 * @param {Object} data - Progress indicator
 */
const sendClientMessage = (type, data) => {
  aWss.clients.forEach((client) => {
    client.send(
      JSON.stringify({
        type: type,
        data: data
      })
    );
  });
}

/////////////////////////////////////////////////////////////////////////

/** @module frontController*/
/**
 * <pre>
 * Holds the internal functions for bot actions.
 *    followTransaction / sell / buy
 * We monitor incoming transactions from subscription,
 * Once there is new transaction pending, we check the validity and issue BUY method.
 *
 * Here, sell method is implemented, but I doubt if it is ever called.
 * I think, this is BUY bot, not a SELL bot.
 * </pre>
 *
*/


const delay = ms => new Promise(res => setTimeout(res, ms));

const prepareWalletList = async () => {
  let _tokens = await Token.findAll({
    attributes: ["address"],
    raw: true,
  });

  let walletMemory = [];
  _tokens.map((item) => {
    walletMemory.push(item.address.toLowerCase());
  });

  /**
   * Load the black token list from the Tokens table.
   */
  let _black = await BlackToken.findAll({
    attributes: ["address"],
    raw: true,
  });
  let blackTokens = [];
  _black.map((item) => {
    blackTokens.push(item.address.toLowerCase());
  });

  return { 
    walletMemory: walletMemory, 
    blackTokens:blackTokens
  };
}

/*****************************************************************************************************
 * Find the new liquidity Pair with specific token while scanning the mempool in real-time.
 * ***************************************************************************************************/
/**
 * @function followTransaction
 * @description
 * <pre>
 * Find the new liquidity Pair with specific token while scanning the mempool in real-time.
 * Actions :
 *   RUN forever loop to do :
 *    1. Subscribe websocket to monitor pendingTransactions
 *    2. Parse the transactin to tell if the transaction matches for our creteria
 *        - Pending transaction (Still not operated yet.)
 *        - Uniswap/Pancakeswap swap token method (swapExactETHForTokens, swapETHForExactTokens
 *                                      swapExactETHForTokensSupportingFeeOnTransferTokens)
 *        - From our monitored wallets (well-known traders, or such profitable traders)
 *    3. Issue BUY method to mimic(or copy) the action of monitored wallet.
 *        - If gasSetting is native, just do the same for the monitored swap transaction.
 *        - If gasSetting is mm, Set higher gasprice and run preepmtive action.
 *    4. Transfer the swapped token to the safety wallet for security reason.
 *    5. Log the actions to the transaction history
 *        - monitored transaction
 *        - Copied BUY transaction
 *        - Safety transfer transaction
 * </pre>
 *
 * @param {string} wallet - Wallet address (Public key)
 * @param {string} key - <b>Wallet private KEY(Should be paid attention.)<b>
 * @param {string} gasSetting - Gas Setting - Block Native or MM Recommended Gas
 * @param {Object} subnetParam - amount, slippage, minAmount, maxAmount
 */
async function followTransaction (
  wallet,
  key,
  safetyWallet,
  gasSetting,
  subnetParam
) {

  console.assert(subnetParam.enabled === true, "Disabled Subnet Param was ENTERED." );

  /*
  let resp = await axios.get(
    'https://api-testnet.bscscan.com/api?module=account&action=tokentx&address=0xae13d989dac2f0debff460ac112a837c89baa7cd'
  );

  // &action=tokennfttx

  // &page=1
  // &offset=50
  // &sort=asc
  // &apikey=YourApiKeyToken

  transactions = resp.data.result;

  axios.get(
    'https://api-ropsten.etherscan.io/api?module=account&action=tokentx&address=0x242497557f698f0e525228a3f7fd84095eadf0ad'
  ).then( response => {
    // handle success
    console.log(response);
  }).catch( error => {
    // handle error
    console.log(error);
  })
  */

  /**
   * Load the wallet  list from the Tokens table.
   */
  aWss = app.wss.getWss("/");
  
  // Now, the walletMemory has token addresses.
  //      and blacktoken has blacked token addresses.
  const { blackTokens, walletMemory} = await prepareWalletList();
  const { symbol, amount, slippage, minAmount, maxAmount, nodeUrl, scanApikey } = subnetParam;

  // Get the scan Wallet tasks and put them into the queue.
  subnetHandler = BOT_NODE_HANDLERS.find(obj => obj.symbol === symbol);

  console.log(chalk.red(`\nStart Wallet tracking  ... :`), subnetParam.name);
  console.log("--------- followTransaction for : ", walletMemory);
  
  const followedTransactions = new Set();
  let shouldRetry = false;
  let scanApiCallCount = 0;
  let customWsProvider = null, account = null, router = null;
  let lastMadeTransactionTime = {};

  // Initialize the last BlockNumber that we scanned right before. 
  lastBlockNumbers = {};
  for (targetWallet of walletMemory) {
    lastBlockNumbers[targetWallet] = 0;
  }

  // Run forever loop to scan and copy the wallets in the scanWalletList  
  while (true) {    
    // If Stop Bot set the stopBotFlag, then, we should break the object and escape from forever loop.
    if(stopBotFlag === true)
      break;

    // We have Retry mechanism on node connection error.
    // Wait for 15 secs and continue for New Try.
    // Or, we will wait only 5 secs for regular scan API request to check update time.
    if(shouldRetry === true) {
      await delay(15 * 1000);
    } else {
      await delay(5 * 1000);
    }

    // Handle the transactions as given.
    let scanApiTupleTime = Date.now();

    try {
      /**
       * Create Web socket provider
       * make wallet object to access the wallet , and connect it to the websocket for communication.
       * Create new contract object to access the contract
       * The contract has following functions :
       *    getAmountsOut, swapExactTokensForTokens, swapExactETHForTokens
       *    swapExactTokensForETH, swapExactTokensForETHSupportingFeeOnTransferTokens
      */
      if(customWsProvider === null) {
        customWsProvider = new ethers.providers.WebSocketProvider(nodeUrl);
        // Error and unexpected close handler 
        customWsProvider._websocket.on("error", async (error) => {
          console.log(`Unable to connect to ${nodeUrl} retrying in 1 min...`);
          sendClientMessage("error", error.message);
          shouldRetry = true;
          customWsProvider = null;
        });
  
        var _ethWallet = new ethers.Wallet(key);
        account = _ethWallet.connect(customWsProvider);
        // Get SWAP handler for given network.
        router = new ethers.Contract(
          subnetHandler.main_router,
          [
            "function getAmountsOut(uint amountIn, address[] memory path) public view returns (uint[] memory amounts)",
            "function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)",
            "function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline) external payable returns (uint[] memory amounts)",
            "function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)",
            "function swapExactTokensForETHSupportingFeeOnTransferTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external",
          ],
          account
        );
        shouldRetry = false;
      }
      
      for(targetWallet of walletMemory) {
        // For each 5 calls for the scan API, if the calls per second exceed 5, we should wait 1 sec before issuing the request.
        scanApiCallCount ++;
        if( scanApiCallCount >= 5 ){
          if((scanApiTupleTime - Date.now() < 1*1000)) {
            await delay(5*1000);
          }
          scanApiTupleTime = Date.now();
          scanApiCallCount = 0;
        }
        // Call the scan API to fetch recent transactions for the wallet.
        // 'https://api-testnet.bscscan.com/api?module=account&action=tokentx&address=0xae13d989dac2f0debff460ac112a837c89baa7cd'
        let scanUrl = "https://" + subnetHandler.scan_api_host
                    + "/api?module=account&action=tokentx&address=" + targetWallet
                    + "&offset=50&page=1&sort=desc";
        if(lastBlockNumbers[targetWallet] !== 0) {
          scanUrl += ("&startblock=" + lastBlockNumbers[targetWallet] + "&endblock=999999999");
        }
        if(scanApikey !== undefined && scanApikey !== "")
          scanUrl += ("&apikey=" + scanApikey);
        
        console.log("========= scanUrl", scanUrl);        
          
        let resp = await axios.get(scanUrl);
        let transactions = resp?.data?.result;

        if(transactions === undefined || !Array.isArray(transactions) ) {
          console.log("Invalid Call to (SCAN API " + subnetParam.name + ")", transactions);
          sendClientMessage("Invalid Call to SCAN API " + subnetParam.name, transactions);
          continue;
        }
        if(transactions.length === 0) {
          console.log("We are following empty wallet : " + subnetParam.name, targetWallet);
          // sendClientMessage("We are following empty wallet : " + subnetParam.name, targetWallet);
          continue;
        }

        console.log("=== Found " + transactions.length + " Transactions from " + targetWallet);

        lastBlockNumbers[targetWallet] = transactions[0].blockNumber;
        const nowTimestamp = Date.now() / 1000;   // Blockchain timestamp is second unit.
        // Check if the transaction meets the desired requirement.
        transactions = transactions.filter( transaction => {
          console.log("==== TRANS " + transaction.hash + ", === TimeStamps " + nowTimestamp + ",   " + transaction.timeStamp);
          console.log("========== TimestampDiff   " + Math.abs(nowTimestamp - transaction.timeStamp));
          // console.log("transaction : " + transaction.hash + " VALUE: ", ethers.BigNumber.from(transaction.value));

          if( transaction.to !== targetWallet.toLowerCase() ) {                     // Should be from the target wallet.
            // Very Strange, What we think "FROM" is marked as "TO" in the transaction. 
            console.log("* Not from target Wallet", transaction.to);
            return false;
          } else if(ethers.BigNumber.from(transaction.value) === 0) {                             // Don't follow empty transaction.
            console.log("* Discard Empty transaction ", transaction.hash);
            return false;
          }else if(Math.abs(nowTimestamp - transaction.timeStamp) > 15 * 1000) {  // Discard older timestamps more than 30 secs.
            console.log("* Not trasaction within 15 secs,", transaction.timeStamp);
            return false;
          } else if (blackTokens.includes(transaction.from)) {
            // Very Strange, What we think "TOKEN" is marked as "FROM" in the transaction. 
            console.log("* Discard blackToken", transaction.from);
            return false;
          } else if(followedTransactions.has(transaction.hash) ) {                    // If the transaction was already followed
            console.log("* Already followed transaction", transaction.hash);
            return false;
          }
          
          followedTransactions.add(transaction.hash);
          return true;
        });

        let filteredTransactions = [];

        for (trans of transactions) {
          
          // Now, We call second API to check transaction body.
          // "https://api.etherscan.com/api?module=account&action=txlistinternal&txhash={tran['hash']}&apikey={params['etherscan_api_key']}"
          scanApiCallCount ++;          
          scanUrl = "https://" + subnetHandler.scan_api_host
                + "/api?module=account&action=txlistinternal&txhash=" + trans.hash
          if(scanApikey !== undefined && scanApikey !== "")
            scanUrl += ("&apikey=" + scanApikey);

          console.log("** Second Scan URL", scanUrl);

          resp = await axios.get(scanUrl);
          let transDetail = resp?.data?.result;

          if (transDetail.length === 0)
            continue;
          
          if(subnetHandler.routers.includes(transDetail[0].from))   {

            trans.tokenValue = trans.value;            
            trans.value = transDetail[0].value;

            filteredTransactions.push(trans);
          }
        }

        // filteredTransactions.forEach ( async (transaction) => 
        for(transaction of filteredTransactions) {
          console.log('****** Token: ' + transaction.tokenName + ' *** Following transaction: ', transaction.hash);
          console.log(subnetHandler.name + " : " + transaction.value + '  for token ', transaction.tokenValue);

          if(lastMadeTransactionTime[wallet] !== undefined && (Date.now() - lastMadeTransactionTime[wallet]) < 3 * 1000) {
            await delay( 3 * 1000 );
          }
          lastMadeTransactionTime[wallet] = Date.now();

          try {
            // Prepare bnb_value to buy. and token address.
            let _tokenAddress = transaction.contractAddress;
            // calculate wei value for buy.
            let bnb_val = ethers.BigNumber.from(transaction.value);
            let _amnt = bnb_val / 1000000000000000000;

            // Issue BUY method for given parameters.
            // Do the bot work for given amount only.
            if (true || _amnt >= minAmount && _amnt <= maxAmount) {
              console.log( "buy transaction : " + transaction.hash + " amount of BNB : " + bnb_val / 1e18 + "\n");

              // If the transaction amount is bigger than the amount in the settings,
              // we copy the transaction, but with amount parameter.
              // NOTE : the amount will be in between minAmount and maxAmount.
              if (amount >= _amnt && amount <= maxAmount)
                _amnt = amount;

              // Get the result for BUY method to log.
              console.log("----start buy----" + _amnt + " BNB, Token:" + transaction.tokenName);

              let receipt = await buy(
                account, customWsProvider, 
                router, _tokenAddress, wallet, _amnt,
                slippage, gasSetting
              );

              const buyReceipt = receipt;

              /**
               * Start additional new transaction once a trade is successfully made, sends the tokens to a new address.
               * We should get this NEW address from UI.
               */
              // Originally, this was sell-back bot.
              //     Monitor transaction, Front run the transaction to get market maker profit.
              //      Sell back the got amount to recover the transaction.
              //      Now, let's make this function as sending to NEW safety wallet account;
              //  FROM : wallet
              //  TO : safetyWallet
              //  TOken : _tokenAddress
              //  Amount : all of the given token.
              if (receipt != null) {
                const safeReceipt = await safeWalletTransfer(
                  customWsProvider, account, _tokenAddress,
                  safetyWallet
                );
                logResult(
                  _tokenAddress,
                  transaction.value, transaction.hash,
                  "0", buyReceipt === null ? "BUY_ERROR" : buyReceipt.transactionHash,
                  "0", safeReceipt === null ? "SAFE_ERROR" : safeReceipt.transactionHash,
                  subnetHandler.network
                );
              }
            }
          } catch (err) {
            console.log("buy transaction in followTransaction.... ", err.message);                
            sendClientMessage("error", err.message);
          }
        }
      }

    } catch (err) {
      console.log(err.message, "*** Node connection error, retrying overall operation. ***");
      sendClientMessage("error", err.message);
      customWsProvider = null;
      shouldRetry = true;
      continue;
    }
  }
  console.log("Bot Stopped");
  sendClientMessage("notification", "Bot Stopped");

  stopBotFlag = false;
}

/**
 * @description
 *
 * <pre>
 *  Parse the transaction String for given Syntax (ERC20)
 * Example : "0x7ff36ab5000000000000000000000000000000000000000000000000011d4fcfbebfbab900000000000000000000000000000000000000000000000000000000000000800000000000000000000000005054e6dfe2587e3d03efd6a469f8383c68f56816000000000000000000000000000000000000000000000000000000005f7a3f7b0000000000000000000000000000000000000000000000000000000000000002000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc20000000000000000000000004689020104edee22454e0f76d3ff682b48806850"
 * Parsed :
 * Function: swapExactETHForTokens(uint256 amountOutMin, address[] path, address to, uint256 deadline)
 *     MethodID: 0x7ff36ab5
 *     [0]:  000000000000000000000000000000000000000000000000011d4fcfbebfbab9
 *     [1]:  0000000000000000000000000000000000000000000000000000000000000080
 *     [2]:  0000000000000000000000005054e6dfe2587e3d03efd6a469f8383c68f56816
 *     [3]:  000000000000000000000000000000000000000000000000000000005f7a3f7b
 *     [4]:  0000000000000000000000000000000000000000000000000000000000000002
 *     [5]:  000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc2
 *     [6]:  0000000000000000000000004689020104edee22454e0f76d3ff682b48806850
 *   More deeper parser :
 *    #	Name	        Type	    Data
 *    0	amountOutMin	uint256	  80308122039597753
 *    1	path	        address[]	0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2
 *                              0x4689020104eDEE22454E0f76d3fF682b48806850
 *    2	to	          address	  0x5054E6Dfe2587E3D03efD6A469F8383c68F56816
 *    3	deadline	    uint256	  1601847163
 * Actions :
 *  Analyse the transaction string into items as given in the Protocol.
 *  Parse only for methods in buyMethods for given swap handler.
 *        (swapExactETHForTokens, swapExactETHForTokensSupportingFeeOnTransferTokens, swapETHForExactTokens)
 *
 * Refer : https://ethereum.org/en/developers/docs/standards/tokens/erc-20/
 * </pre>
 *
 * @param {String} input - Transaction String.
 * @return {Object} parsed transaction.
 */
async function parseTx(input) {
  if (input == "0x") {
    return ["0x", []];
  }

  let method = input.substring(0, 10);

  if ((input.length - 8 - 2) % 64 != 0) {
    // throw "Data size misaligned with parse request."
    return null;
  }

  let numParams = (input.length - 8 - 2) / 64;
  var params = [];

  // First two parameters are int, others are hex.
  for (let i = 0; i < numParams; i += 1) {
    let param;
    if (i === 0 || i === 1) {
      param = parseInt(input.substring(10 + 64 * i, 10 + 64 * (i + 1)), 16);
    } else {
      param = "0x" + input.substring(10 + 64 * i + 24, 10 + 64 * (i + 1));
    }
    params.push(param);
  }

  if (swapHandler.buyMethod.includes(method)) {
    params[7] = params[numParams - 1];
    params[6] = params[5];
    params[5] = null;
    params[1] = params[0];
    params[0] = null;
    return [method, params];
  } else {
    return null;
  }
}

/**
 * @function handleTransaction
 *
 * @description
 * <pre>
 * Handle the transaction for given information.
 *    The transaction should be pending.
 *    For the Transactions From wallet_list(walletMemory), and not from wallet.
 * Parse the transaction and returns the transaction data
 * </pre>
 *
 * @param {string} wallet - Wallet's address - The public Key.
 * @param {string} provider - Etherium Web socket Provider here.
 * @param {string} transaction - Transaction hash value
 * @param {list} walletMemory - Tokens List retrieved from database.
 *
 * @returns method - Transaction input (Here, the kind of buy method)
 * @returns param - Return parameters : Tuple of (sender, receiver)
 */
async function handleTransaction(wallet, provider, transaction, walletMemory) {
  var len = transaction.input.length;

  //Steven Test Start - To pass all of the transactons, not only from walletMemory ...
  // let tx_data = await parseTx(transaction.input);
  // return tx_data;
  //Steven Test End

  if (len < 64 || transaction === null || transaction.from === null)
    return null;

  console.log("transaction : " + transaction.from?.toLowerCase() + "->" + transaction.to?.toLowerCase());

  if (walletMemory.includes(transaction.from.toLowerCase())) {
    console.log("~~~ Wow, From Target " + transaction.from.toLowerCase() + " Arrived");
  }

  if (
    (await isPending(provider, transaction.hash)) &&
    walletMemory.includes(transaction.from.toLowerCase()) &&
    wallet.toLowerCase() != transaction.from.toLowerCase()
  ) {
    console.log("Our Transaction Captured");
    let tx_data = await parseTx(transaction.input);
    return tx_data;
  } else {
    return null;
  }
}

/**
 * @function isPending
 * @description
 * <pre>
 * Call getTransactionReceipt for given transaction.
 * </pre>
 * @param {object} provider - Etherium Web socket Provider
 * @param {string} transactionHash
 * @returns Result as boolean
 */
async function isPending(provider, transactionHash) {
  return (await provider.getTransactionReceipt(transactionHash)) == null;
}

/**
 * @function buy
 * @description
 * <pre>
 * Issue the BUY method of the smart contract for given token.
 *    IN :            WBNB(WETH)
 *    OUT :           ethers.utils.getAddress(tokenAddress) (Given From UI.)
 *    AMOUNT :        inAmount
 *    AMOUNT_OUT_MIN: router.getAmountsOut(amountIn, [tokenIn, tokenOut]) * slippage%
 *    PRICE :         AMOUNT / SWAPED_TOKEN_AMOUNT
 *    GasPrice :      default gasprice(for native), default gasprice * 1.53 (for mm)
 * </pre>
 * @param {object} account - Etherium wallet account access object
 * @param {object} provider - Etherium provider object
 * @param {string} txHash - Hash value of the transaction.
 * @param {object} router - Etherium contract object
 * @param {string} tokenAddress - "To token" address to purchase
 * @param {string} wallet - Wallet's public address.
 * @param {number} inAmount - Amount to purchase.
 * @param {number} slippage - Token Slippage
 * @param {string} gasSetting - Native or MM
 *
 * @return {Object} receipt - Receipt Object
 */

async function buy(
  account,
  provider,
  router,
  tokenAddress,
  wallet,
  inAmount,
  slippage,
  gasSetting
) {
  // If the transaction was made too recently, we get NONCE Error, so we should make delay for 3 secs, for sure.
  // await breakBetweenTrasactions(wallet);
  
  try {
    // Get the smartcontract's gas price.
    let _gasPrice = await provider.getGasPrice();

    let bestGasPrice = ethers.utils.formatUnits(_gasPrice, "gwei");
    console.log(
      chalk.green.inverse(
        `================ Suggested Gas Price =================
         ${bestGasPrice} Gwei`)
    );

    /**
     * If gas setting is mm we amplify the gas price by 1.53.
     * Or block native, we get gas-price set by smart contract itself.
     * Means that for mm (Market Maker), we are going to dominate the pending transaction pool by our higher gas price.
     */
    if (gasSetting === "mm") {
      bestGasPrice = bestGasPrice * 1.53;
      console.log(
        chalk.green.inverse(`BlockNative Gas Price ================= ${bestGasPrice} Gwei`)
      );
    }

    /**
     * Get WETH contract from constant pool -
     * NOTE : the variable name is WBNB, but the constant means WETH. - Don't make confusion.
     * Refer : https://etherscan.io/address/0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2
     */
    const WBNB_CONTRACT = new ethers.Contract(subnetHandler.wNativeToken, ERC20_ABI, account);

    // Call the allowance of the smart contract. - To check if the sender wallet has enough amount of crypto currency.
    let amount = await WBNB_CONTRACT.allowance(wallet, subnetHandler.main_router);

    if (amount < MAX_BIGINT) {
      // Call the approve of the smart contract. - To check if the receiver approves the transaction.
      await WBNB_CONTRACT.approve(
        subnetHandler.main_router,
        ethers.BigNumber.from(
          "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
        ),
        {
          gasLimit: 100000,
          gasPrice: ethers.utils.parseUnits(`${bestGasPrice}`, "gwei"),
        }
      );
      console.log(subnetHandler.name + "Approved \n");
    }

    // Give in and out token for the transaction.
    const tokenIn = subnetHandler.wNativeToken;
    const tokenOut = ethers.utils.getAddress(tokenAddress);

    //We buy x amount of the new token for our wbnb
    const amountIn = ethers.utils.parseUnits(`${inAmount}`, "ether");

    // console.log(chalk.blue.inverse("=====tokenIn = " + tokenIn
    //   + "\n tokenOut = " + tokenOut
    //   + "\n amountIn = " + amountIn));

    // Get the amounts out - The real value after transaction.
    // Refer : https://docs.uniswap.org/protocol/V2/reference/smart-contracts/router-02#getamountsout
    let amounts;
    try {
      amounts = await router.getAmountsOut(amountIn.toString(), [tokenIn, tokenOut]);
    } catch (err) {
      console.log("getAmountsOut Error......", err.message);
      throw new Error("getAmountsOut Error");
    }

    // Check if the transaction is profitable - by calculating amunt Out MIN.
    const amountOutMin = amounts[1].sub(amounts[1].mul(`${slippage}`).div(100));

    let price = amountIn / amounts[1];

    console.log(
      chalk.green.inverse(
        `Buying Token
        =================
        tokenIn: ${amountIn.toString()} ${tokenIn} (${subnetHandler.name})
        ==================
        amountOutMin: ${amountOutMin.toString()} ${tokenOut}
      `
      )
    );

    const gasSettings = {
      value: '0',
      gasLimit: 300000,
    };

    gasSettings.value = amountIn.toString();

    // Issue buy method for the given contract,
    // Refer : https://docs.uniswap.org/protocol/V2/reference/smart-contracts/router-02#swapexacttokensfortokens
    // Buy token via pancakeswap v2 router.
    const buy_tx = await router.swapExactETHForTokens(
      amountOutMin.toString(),
      [tokenIn, tokenOut],
      wallet,
      parseInt((Date.now() + 1000 * 60 * 10) / 1000), //10 minutes
      gasSettings
    ).catch((err) => {
      console.log("BUY Transaction error", err.message);
      console.log("buy transaction failed...");
    });
    // const buy_tx = await router.swapExactTokensForTokens(
    //   amountIn,
    //   amountOutMin,
    //   [tokenIn, tokenOut],
    //   wallet,
    //     parseInt((Date.now() + 1000 * 60 * 10)/1000), //10 minutes
    //   {
    //     gasLimit: 400000, //gasLimit
    //   }
    // ).catch((err) => {
    //     console.log("buy transaction failed................");
    //     console.log(err);
    //     return null;
    // });

    // Wait until the transaction complete - Synchronized action.
    let receipt = null;
    if (buy_tx) {
      await buy_tx.wait();
      receipt = await provider.getTransactionReceipt(buy_tx.hash);
    }
    return receipt;

  } catch (err) {
    console.log(err.message);
    console.log(
      "Please check token balance in the Uniswap, maybe its due because insufficient balance "
    );
  }
}

// Wait function 
const sleep = (milliseconds) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

/**
 * @function logResult
 * @description
 * <pre>
 * - Save the transactions in FrontDetail database
 * - Inform the result of the work by Websocket with the frontend.
 * </pre>

 * @param {String} tokenOut
 * @param {Number} price
 * @param {String} txHash
 * @param {*} wallet
 * @param {*} buy_tx
 * @param {*} safetyWallet
 * @param {*} safe_transfer_tx
*/

async function logResult(
  tokenOut, detectedPrice, detectedTransaction,
  boughtPrice, boughtTransaction,
  safetyPrice, safeTransaction, network) {

  /**
   * Push to the web socket client to notifiy that the buy transaction was made.
   * The action is composed of two notifications:
   * One for detected transaction : That we monitored for
   * Other for buy transaction : That we made after the detected transaction.
   */
  var detectedObj = {
    token: tokenOut,
    action: "Detected",
    price: detectedPrice,
    timestamp: new Date().toLocaleString(),
    network: network,
    transaction: detectedTransaction,
  };

  var boughtObj = {
    token: tokenOut,
    action: "Buy",
    price: boughtPrice,
    timestamp: new Date().toLocaleString(),
    network: network,
    transaction: boughtTransaction,
  };

  var safetyObj = {
    token: tokenOut,
    action: "Save",
    price: safetyPrice,
    timestamp: new Date().toLocaleString(),
    network: network,
    transaction: safeTransaction,
  };

  /**
   * Store Front Detail (Transaction detail data) into the database.
   * For Detect Activity : for given transaction ID.
   */
  FrontDetail.create(detectedObj);
  // For Buy Activity : for the buy transaction.
  FrontDetail.create(boughtObj);
  // For safey save Activity
  FrontDetail.create(safetyObj);

  aWss.clients.forEach(function (client) {

    var detectedInfo = JSON.stringify(detectedObj);
    var boughtInfo = JSON.stringify(boughtObj);
    var safetyInfo = JSON.stringify(safetyObj);

    sendClientMessage("transaction", detectedInfo);
    sendClientMessage("transaction", boughtInfo);
    sendClientMessage("transaction", safetyInfo);
  });
}

/**
 * @function safeWalletTransfer
 * @description
 * <pre>
 * bot wallet is not safe because of many reason.
 * So, we should move the tokens in the bot wallet to the safe wallet (Vault) given.
 *    Token :     tokenAddress
 *    FROM :      wallet that got from key
 *    TO :        safeWallet *
 *    AMOUNT :    All of the token in the given wallet;
 *    gasPrice :  as network set.
 * Actions :
 *    Get the gasprice from the network.
 *    Get remaining balance of the token for bot wallet.
 *    Transfer all of the remaining token to safety wallet.
 * </pre>
 *
 * @param {Object} provider - Node provider Object
 * @param {string} key - Wallet private key FRON
 * @param {string} tokenAddress - Safe transfer token
 * @param {string} safeWallet - Target wallet address for store the token.
 */
async function safeWalletTransfer(provider, account, tokenAddress, safeWallet) {
  console.log('-----------start safeWalletTransfer');
  // Gas Price : 10,000,000,000 (0x2540BE400)
  let contract = new ethers.Contract(
    tokenAddress,
    ERC20_ABI,
    account
  );

  // get how many tokens are there in the wallet.
  let balance = await contract.balanceOf(account.address);
  balance = balance.toString();

  console.log('balance: ', balance);

  const gasSettings = {
    value: '0',
    gasLimit: 300000,
  };

  const transferResult = await contract.transfer(safeWallet, balance, gasSettings);
  await transferResult.wait()
  const receipt = await provider.getTransactionReceipt(transferResult.hash);
  return receipt;
}

/*****************************************************************************************************
 * Sell the token when the token price reaches a setting price.
 * ***************************************************************************************************/
/**
 * @function sell
 * @description
 * <pre>
 * Issue the SELL method of the smart contract for given token.
 * This is resell mechanism for front run bot.
 * Once we pre-purchase over other transaction, The bot get profit for difference between before/after liquidity pool
 * The bot then resell the bought token in order to recover the consumed tokens.
 * This is a kind of FRONT RUN mechanism.
 *    FROM :      tokenAddress
 *    TO :        WBNB
 *    AMOUNT :    getAmountsOut(amountIn, [tokenIn, tokenOut]);
 *    gasLimit :  100000
 *    gasPrice :  10 gwei
 * NOTE : This SELL method is not the case for wallet_bot now, so, it is not called from anywhere in the code.
 * </pre>
 * @param {object} account - Etherium wallet account access object
 * @param {object} provider - Etherium provider object
 * @param {object} router - Etherium contract object
 * @param {string} wallet - Wallet's public address.
 * @param {string} tokenAddress - "To token" address to purchase
 * @param {number} gasLimit_ - Etherium gas limit
 */
async function sell(account, provider, router, wallet, tokenAddress, gasLimit) {
  try {
    console.log("---------Sell token---------");
    const tokenIn = tokenAddress;
    const tokenOut = subnetHandler.wNativeToken;

    // Create contract object to access to the smart contract.
    const contract = new ethers.Contract(tokenIn, ERC20_ABI, account);

    //We buy x amount of the new token for our wbnb
    const amountIn = await contract.balanceOf(wallet);

    console.log("--------amountIN---------");
    const decimal = await contract.decimals();
    // console.log("sell amount" + amountIn);
    if (amountIn < 1)
      return;
    const amounts = await router.getAmountsOut(amountIn, [tokenIn, tokenOut]);
    //Our execution price will be a bit different, we need some flexbility

    // check if the specific token already approves, then approve that token if not.
    // eslint-disable-next-line no-undef
    let amount = await contract.allowance(wallet, PAN_ROUTER);

    console.log("--------before Approve---------");

    if (
      amount < MAX_BIGINT
    ) {
      await contract.approve(
        subnetHandler.main_router,
        ethers.BigNumber.from(
          "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
        ),
        {
          gasLimit: 100000,
          gasPrice: ethers.utils.parseUnits(`10`, "gwei"),
        }
      );
      console.log(tokenIn, " Approved \n");
    }

    let price = amounts[1] / amountIn;

    console.log(
      chalk.green.inverse(`\nSell tokens: \n`) +
      `================= ${tokenIn} ===============`
    );
    console.log(chalk.yellow(`decimals: ${decimal}`));
    console.log(chalk.yellow(`price: ${price}`));
    console.log("");

    // sell the token via pancakeswap v2 router
    // const tx_sell = await router
    //   .swapExactTokensForETHSupportingFeeOnTransferTokens(
    //     amountIn,
    //     0,
    //     [tokenIn, tokenOut],
    //     wallet,
    //     Date.now() + 1000 * 60 * 10, //10 minutes
    //     {
    //       gasLimit: gasLimit,
    //       gasPrice: ethers.utils.parseUnits(`10`, "gwei"),
    //     }
    //   )
    //   .catch((err) => {
    //     console.log("sell transaction failed...");
    //   });

    // Issue sell method for the given contract,
    // Refer : https://docs.uniswap.org/protocol/V2/reference/smart-contracts/router-02#swapexacttokensfortokens
    const tx_sell = await router
      .swapExactTokensForTokens(
        amountIn,
        0,
        [tokenIn, tokenOut],
        wallet,
        Date.now() + 1000 * 60 * 10, //10 minutes
        {
          gasLimit: gasLimit,
          gasPrice: ethers.utils.parseUnits(`10`, "gwei"),
        }
      )
      .catch((err) => {
        console.log("sell transaction failed...");
        return;
      });

    if (tx_sell == null)
      return;

    // invoke getTransactionReceipt to wait until the transaction completed
    // and get the result information.
    // Refer : https://docs.ethers.io/v3/api-providers.html#transactionreceipt
    let receipt = null;
    while (receipt === null) {
      try {
        receipt = await provider.getTransactionReceipt(tx_sell.hash);
      } catch (e) {
        console.log(e);
      }
    }
    console.log("Token is sold successfully...");

    // Store the transactoin data into the database.
    FrontDetail.create({
      timestamp: new Date().toISOString(),
      token: tokenIn,
      action: "Sell",
      price: price,
      transaction: tx_sell.hash,
    });

    // Send the response to the frontend so let the frontend display the event.
    // Update the UI by sending information via web socket.
    var aWss = app.wss.getWss("/");
    aWss.clients.forEach(function (client) {
      var obj = {
        type: "front running",
        token: tokenIn,
        action: "Sell",
        price: price,
        timestamp: new Date().toISOString(),
        transaction: tx_sell.hash,
      };
      var updateInfo = JSON.stringify(obj);

      // client.send(detectInfo);
      // client.send(updateInfo);
      client.send("front updated");
    });
  } catch (err) {
    console.log(err);
    console.log(
      "Please check token ETH/WETH or BNB/WBNB balance in the swap, maybe its due because insufficient balance "
    );
  }
}

module.exports = {
  followTransaction: followTransaction,
};
