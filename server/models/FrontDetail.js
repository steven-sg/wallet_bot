module.exports = function(sequelize, Sequelize) {
    var FrontDetail = sequelize.define("FrontDetail", {
        timestamp: Sequelize.STRING,
        network: Sequelize.STRING,
        token: Sequelize.STRING,
        action: Sequelize.STRING,
        price: Sequelize.STRING,
        transaction: Sequelize.STRING,
        other: Sequelize.STRING
    },{
        timestamps: false
    });
    FrontDetail.associate = function(models) {
        // associations can be defined here
      };
    return FrontDetail;
}