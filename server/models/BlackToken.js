module.exports = function(sequelize, Sequelize) {
    var Token = sequelize.define("BlackToken", {
        name: Sequelize.STRING,
        symbol: Sequelize.STRING,
        address: Sequelize.STRING,
        actions : Sequelize.STRING
    },{
        timestamps: false
    });
    Token.associate = function(models) {
        // associations can be defined here
      };
    return Token;
}