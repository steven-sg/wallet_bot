module.exports = function(sequelize, Sequelize) {
    var Front = sequelize.define("Front", {
        status: Sequelize.STRING,
        wallet: Sequelize.STRING,
        key: Sequelize.STRING,
        safetyWallet: Sequelize.STRING,
        // token: Sequelize.STRING,
        gasSetting: Sequelize.STRING,
    },{
        timestamps: false
    });
    Front.associate = function(models) {
        // associations can be defined here
      };
    return Front;
}