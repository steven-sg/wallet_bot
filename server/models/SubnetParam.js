
module.exports = function(sequelize, Sequelize) {
    var SubnetParam = sequelize.define("SubnetParam", {
        enabled: Sequelize.BOOLEAN,
        name: Sequelize.STRING,
        acronym: Sequelize.STRING,
        symbol: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        scanApikey: Sequelize.STRING,
        nodeUrl: Sequelize.STRING,
        amount: Sequelize.STRING,
        slippage: Sequelize.STRING,
        minAmount: Sequelize.STRING,
        maxAmount: Sequelize.STRING,
    },{
        timestamps: false
    });
    SubnetParam.associate = function(models) {
        // associations can be defined here
      };
    return SubnetParam;
}