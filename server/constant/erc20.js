
require('dotenv').config();

const ERC20_ABI = [
  {
    constant: false,
    inputs: [
      { name: '_spender', type: 'address' },
      { name: '_value', type: 'uint256' },
    ],
    name: 'approve',
    outputs: [{ name: 'success', type: 'bool' }],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    constant: true,
    inputs: [],
    name: 'totalSupply',
    outputs: [{ name: 'supply', type: 'uint256' }],
    payable: false,
    stateMutability: 'view',
    type: 'function',
  },
  {
    constant: false,
    inputs: [
      { name: '_from', type: 'address' },
      { name: '_to', type: 'address' },
      { name: '_value', type: 'uint256' },
    ],
    name: 'transferFrom',
    outputs: [{ name: 'success', type: 'bool' }],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    constant: true,
    inputs: [],
    name: 'decimals',
    outputs: [{ name: 'digits', type: 'uint256' }],
    payable: false,
    stateMutability: 'view',
    type: 'function',
  },
  {
    constant: true,
    inputs: [{ name: '_owner', type: 'address' }],
    name: 'balanceOf',
    outputs: [{ name: 'balance', type: 'uint256' }],
    payable: false,
    stateMutability: 'view',
    type: 'function',
  },
  {
    constant: false,
    inputs: [
      { name: '_to', type: 'address' },
      { name: '_value', type: 'uint256' },
    ],
    name: 'transfer',
    outputs: [{ name: 'success', type: 'bool' }],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function',
  },
  {
    constant: true,
    inputs: [
      { name: '_owner', type: 'address' },
      { name: '_spender', type: 'address' },
    ],
    name: 'allowance',
    outputs: [{ name: 'remaining', type: 'uint256' }],
    payable: false,
    stateMutability: 'view',
    type: 'function',
  },
  {
    anonymous: false,
    inputs: [
      { indexed: true, name: '_owner', type: 'address' },
      { indexed: true, name: '_spender', type: 'address' },
      { indexed: false, name: '_value', type: 'uint256' },
    ],
    name: 'Approval',
    type: 'event',
  },
];


// Metamask SWAP address : 0x881D40237659C251811CEC9c364ef91dC08D300C
// UNISWAP Ropsten : 0x9c83dCE8CA20E9aAF9D3efc003b2ea62aBC08351

const BOT_NODE_HANDLERS_TEST = [
  // ============================ UNISWAP ============================ //
  { // UNISWAP_V2_TEST - ROPSTEN
    network: "Ethernet",
    symbol: "ETH",
    mode: "test",
    name: "wETH",
    wNativeToken: '0xc778417e063141139fce010982780140aa0cd5ab',
    node: "wss://speedy-nodes-nyc.moralis.io/db438a88c8bfbef7505bae0b/eth/ropsten/ws",
    scan_api_host: "api-ropsten.etherscan.io",
    main_router: "0x7a250d5630b4cf539739df2c5dacb4c659f2488d",
    routers: [
     "0x7a250d5630b4cf539739df2c5dacb4c659f2488d",   // "UNISWAP_V2_ROPSTEN",
    ],
  },

  // ============================ PANCAKESWAP TEST ============================ //
  { // PANCAKESWAP_V2_TEST  - bsctestnet
    network: "BSC",
    symbol: "BNB",
    mode: "test",
    name: "wBNB",
    wNativeToken: '0xae13d989dac2f0debff460ac112a837c89baa7cd',
    node: "wss://speedy-nodes-nyc.moralis.io/db438a88c8bfbef7505bae0b/bsc/testnet/ws",
    scan_api_host: "api-testnet.bscscan.com",
    main_router: "0x9ac64cc6e4415144c455bd8e4837fea55603e5c3",
    routers: [
      "0x9ac64cc6e4415144c455bd8e4837fea55603e5c3",   // "PANCAKESWAP_V2_TEST",
    ],
  },
];

BOT_NODE_HANDLERS_PRODUCTION = [
  // ============================ UNISWAP ============================ //
  { // UNISWAP_PRODUCTION - MAINNET
    network: "Ethernet",
    symbol: "ETH",
    mode: "production",
    name: "wETH",
    wNativeToken: '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2',
    node: "wss://speedy-nodes-nyc.moralis.io/db438a88c8bfbef7505bae0b/eth/mainnet/ws",
    scan_api_host: "api.etherscan.io",
    main_router: "0x7a250d5630b4cf539739df2c5dacb4c659f2488d",
    routers: [
      "0x7a250d5630b4cf539739df2c5dacb4c659f2488d",   // "UNISWAP_V2",
      "0xe592427a0aece92de3edee1f18e0157c05861564",   // "UNISWAP_V3_R1",
      "0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45",   // "UNISWAP_V3_R2",
    ],
  },

  // ============================ PANCAKESWAP ============================ //
  { // PANCAKESWAP_V2  - bscnet
    network: "BSC",
    symbol: "BNB",
    mode: "production",
    name: "wBNB",
    wNativeToken: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c',
    node: "wss://speedy-nodes-nyc.moralis.io/db438a88c8bfbef7505bae0b/bsc/mainnet/ws",
    scan_api_host: "api.bscscan.com",
    main_router: "0x10ed43c718714eb63d5aa57b78b54704e256024e",
    routers: [
      "0x10ed43c718714eb63d5aa57b78b54704e256024e",   // "PANCAKESWAP_V2",
    ],
  },
];



// Max big Integer 2^256 - 1;
const MAX_BIGINT = 115792089237316195423570985008687907853269984665640564039457584007913129639935;
// 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff

const BOT_NODE_HANDLERS = (process.env.REACT_APP_RUN_ENV === "PRODUCTION")
  ? BOT_NODE_HANDLERS_PRODUCTION
  : BOT_NODE_HANDLERS_TEST;

  module.exports = {
  ERC20_ABI,
  BOT_NODE_HANDLERS,
  MAX_BIGINT,
};