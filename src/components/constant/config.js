
require('dotenv').config();

console.log(process.env);
const CONFIG_TEST = {
  RUNNING_MODE: "test",
  NODE_URL: 'wss://mainnet.infura.io/ws/v3/2265cf2c03c442e4b0fe3b4e6e667e87', //https://mainnet.infura.io/v3/2265cf2c03c442e4b0fe3b4e6e667e87
  EXPLORER: 'https://testnet.bscscan.com/tx/',
  EXPLORER_ADDR: 'https://testnet.bscscan.com/address/',
}

const CONFIG_PRODUCTION = {
  RUNNING_MODE: "production",  
  NODE_URL: 'wss://mainnet.infura.io/ws/v3/2265cf2c03c442e4b0fe3b4e6e667e87', //https://mainnet.infura.io/v3/2265cf2c03c442e4b0fe3b4e6e667e87
  EXPLORER: 'https://bscscan.com/tx/',
  EXPLORER_ADDR: 'https://bscscan.com/address/',
}

const CONFIG = (process.env.REACT_APP_RUN_ENV === "PRODUCTION") ? CONFIG_PRODUCTION : CONFIG_TEST;

export default CONFIG