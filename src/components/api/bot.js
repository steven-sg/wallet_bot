import { errorMessage, createNotification, client } from "./api";

/**
 * Call bot actions 
 * See backend documentation - 
 * Simple API wrapper for calling the backend API.
 * @param {String} node    - Node URL
 * @param {String} network - Ethereum or Binance
 * @param {String} wallet  - Wallet Address
 * @param {String} key     - Wallet Private Key
 * @param {String} safetyWallet - Safety Wallet to save the bought tokens.
 * @param {String} token   - Token to trade.
 * @param {String} gasSetting - Network itself, or Markete Maker
 * @param {Object} subnetParam - Parameter for amount, slippage, minAmount, maxAmount
 */
export async function startFront(
  wallet, key, safetyWallet, 
  gasSetting,
  subnetParams) {

  try {
    await client.post('bots/startFront', {
      wallet:wallet,
      key : key,
      safetyWallet : safetyWallet,
      gasSetting: gasSetting,
      subnetParams: subnetParams,
    });
  } catch (err) {
    createNotification("error", errorMessage(err));
  }
}

export async function stopFront(subnetParam) {
  try {
    await client.post('bots/stopFront', {
      subnetParam: subnetParam,
    });
  } catch (err) {
    createNotification("error", errorMessage(err));
  }
}

export async function getFrontStatus() {
try {
  let res =  await client.get('bots/getFrontStatus');
  let status = res.data.data;
  return status;
} catch (err) {
  createNotification("error", errorMessage(err));
}
}