import React, { useState, useEffect, useMemo } from "react";
import { Button, FormControl } from "react-bootstrap";
import { MDBDataTable } from "mdbreact";

import { useDispatch, useSelector } from 'react-redux';

import { w3cwebsocket as W3CWebSocket } from "websocket";
import "./Display.css";
import { createNotification, startFront, stopFront, getFrontStatus, listFront } from "./api";
import CONFIG from "./constant/config";

import { setSubnetParam, setSubnet, selectSubnet } from '../redux';

require('dotenv').config();


/**
 * Basic UI module for handling Bot Operation.
 * Detailed comments are seen below.
*/

// Data Storages for app work. 

const client = new W3CWebSocket("ws://localhost:8080/connect");
// const client = new W3CWebSocket("ws://136.244.78.15/api/connect");


const MainSettings = (props) => {

  const subnet = useSelector(selectSubnet);

  const [walletAddress, setWalletAddress] = useState("");
  const [privateKey, setPrivateKey] = useState("");
  const [safetyWallet, setSafetyWallet] = useState("");
  const [gasSetting, setGasSetting] = useState("native");

  // Run the backend bot by pressing "Start Bot" toggle button.
  const start = () => {
    if (
      privateKey === ""
    ) {
      alert("please input all information to start wallet tracking !");
    } else {

      // replaceWith();
      props.setRunning(true);
      startFront(
        walletAddress.trim(),
        privateKey.trim(),
        safetyWallet.trim(),
        gasSetting,
        subnet,
      );
    }
  };

  // Stop the Bot as pressing "Stop Bot" toggle button  
  const stop = () => {
    props.setRunning(false);
    stopFront(subnet);
  };

  // Load Settings from the backend database
  // The UI fields accepts the setting values to show in their boxes.
  const dispatch = useDispatch();
  const setAllSubnetParam = (param) => {
    param.forEach((subnet) => {
      dispatch(setSubnet(subnet));
    });
  };

  const loadSetting = (status) => {
    setWalletAddress(status.wallet);
    setPrivateKey(status.key);
    setSafetyWallet(status.safetyWallet);
    setGasSetting(status.gasSetting);
    setAllSubnetParam(status.subnetParam);
  };


  const setStatus = async () => {
    var curStatus = await getFrontStatus();
    if (curStatus === undefined)
      return;
    loadSetting(curStatus);
    props.setRunning(curStatus.status === "1" ? true : false);
  };


  /**
   * On loading 
   * Get the status(parameters) of the app.
   * Get the list of the transactions 
   * to show to the UI.
   */
  useEffect(() => {
    setStatus();
  }, []);

  return (
    <div>
      <div className="row">
        <div className="form-group col-sm-12 col-md-6">
          <label htmlFor="usr">Bot Wallet Address</label>
          <FormControl
            type="text"
            id="walletAddr"
            value={walletAddress}
            onChange={(e) => {
              setWalletAddress(e.target.value);
            }}
            className="form-control form-control-md"
          />
        </div>

        <div className="form-group col-sm-12 col-md-6">
          <label htmlFor="pwd"> Bot Private Key </label>
          <FormControl
            type="password"
            id="privateKey"
            value={privateKey}
            onChange={(e) => {
              setPrivateKey(e.target.value);
            }}
            className="form-control form-control-md"
          />
        </div>
      </div>
      <div className="row">
        <div className="form-group col-sm-12 col-md-6">
          <label htmlFor="safetyWallet"> Safety Wallet Address</label>
          <FormControl
            type="text"
            id="safetyWallet"
            value={safetyWallet}
            onChange={(e) => {
              setSafetyWallet(e.target.value);
            }}
            className="form-control form-control-md"
          />
        </div>

        <div className="form-group col-sm-12 col-md-6">
          <div className="row">
            <div className="form-group col-sm-12 col-md-3">
              <label htmlFor="pwd" className="section_gasSetting_title">
                Gas Setting &nbsp;
              </label>
            </div>

            <div className="form-group col-sm-12 col-md-5">
              <span className="section_gasSetting_radio">
                <label>
                  <input
                    type="radio"
                    value="native"
                    checked={gasSetting === "native"}
                    onChange={(e) => {
                      setGasSetting("native");
                    }}
                  />
                  &nbsp; Block Native
                </label>
                <label>
                  <input
                    type="radio"
                    value="mm"
                    checked={gasSetting === "mm"}
                    onChange={(e) => {
                      setGasSetting("mm");
                    }}
                  />
                  &nbsp; MM Recommended
                </label>
              </span>
            </div>

            <div className="form-group col-sm-12 col-md-4">
              <Button
                variant={props.running ? "danger" : "primary"}
                className="btn_start"
                onClick={props.running ? () => stop() : () => start()}
              >
                {props.running ? "STOP BOT" : "START BOT"}
              </Button>
            </div>
          </div>
        </div>
      </div>

    </div>
  );
}

const SubNetwork = (props) => {

  const subnet = useSelector(selectSubnet);
  var param = subnet[props.symbol];

  const dispatch = useDispatch();
  const changeSubnetParam = (symbol, target, value) => {
    dispatch(setSubnetParam({ symbol, target, value }));
  }

  return (
    <div>
      <div className="row">
        <div>
          <input className="mr-4"
            type="checkbox" value={param.enabled ? "checked" : ""} name="enabled"
            checked={param.enabled}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "enabled", e.target.checked);
            }}
          />
          <label htmlFor="enable"> {param.name} </label>
        </div>
      </div>
      <div className="row">
        <div className="form-group col-sm-12 col-md-6">
          <label htmlFor="wssURL"> {param.name} Scan API Key: </label>
          <FormControl
            type="text"
            value={param.scanApikey}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "scanApikey", e.target.value);
            }}
            className="form-control form-control-md"
          />
        </div>

        <div className="form-group col-sm-12 col-md-6">
          <label htmlFor="wssURL"> {param.acronym} Node WSS URL:</label>
          <FormControl
            type="text"
            value={param.nodeUrl}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "nodeUrl", e.target.value);
            }}
            className="form-control form-control-md"
          />
        </div>
      </div>

      <div className="row">
        <div className="form-group col-sm-12 col-md-3">
          <label htmlFor="amount"> {param.symbol} amount </label>
          <input
            type="number"
            id="amount"
            className="short-input"
            value={param.amount}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "amount", e.target.value);
            }}
          />
        </div>

        <div className="form-group col-sm-12 col-md-3">
          <label htmlFor="pwd">Slippage(%) &nbsp;&nbsp;&nbsp;</label>
          <input
            type="number"
            id="slippage"
            className="short-input"
            value={param.slippage}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "slippage", e.target.value);
            }}
          />
        </div>

        <div className="form-group col-sm-12 col-md-3">
          <label htmlFor="pwd">Min {param.symbol} to follow</label>
          <input
            type="number"
            id="minAmount"
            className="short-input"
            value={param.minAmount}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "minAmount", e.target.value);
            }}
          />
        </div>

        <div className="form-group col-sm-12 col-md-3">
          <label htmlFor="pwd">Max {param.symbol} to follow</label>
          <input
            type="number"
            id="maxAmount"
            className="short-input"
            value={param.maxAmount}
            onChange={(e) => {
              changeSubnetParam(param.symbol, "maxAmount", e.target.value);
            }}
          />
        </div>
      </div>
    </div>
  );
}

const Transactions = (props) => {

  /**
   * Get the transactions from server to show the list.
   * Actions for the list : Check from eth/bsc scan.
   */
  var transactionItems = [];
  const [transactions, setTransactions] = useState([]);

  const listTransactions = async () => {
    transactionItems = await listFront();
    setTransactions(transactionItems);
  };

  const rows = useMemo(() => {
    return transactions.map((item) => {
      const newItem = { ...item };
      newItem.transaction = (
        <a href={CONFIG.EXPLORER + newItem.transaction} target="_blank" rel="noopener noreferrer">
          {newItem.transaction}
        </a>
      );
      return newItem;
    })
  }, [transactions]);

  const data = {
    columns: [
      {
        label: "TimeStamp",
        field: "timestamp",
      },
      {
        label: "Network",
        field: "network",
      },      
      {
        label: "Token",
        field: "token",
      },
      {
        label: "Buy/Sell",
        field: "action",
      },
      // {
      //   label: "Price",
      //   field: "price",
      // },
      {
        label: "Transaction",
        field: "transaction",
      },
    ],
    rows: rows,
  };

  const showNotification = (type, message) => {
    createNotification(type, message);
  }
  useEffect(() => {
    listTransactions();
    client.onopen = () => {
      console.log("WebSocket Client Connected");
    };

    client.onmessage = (message) => {
      const msgData = JSON.parse(message.data);
      const data = msgData.data;
      const type = msgData.type;
      switch (type) {
        case "transaction":
          const trans = JSON.parse(data);
          setTransactions(prevTransactions =>
            [
              {
                timestamp: trans.timestamp,
                token: trans.token,
                action: trans.action,
                transaction: trans.transaction,
              },
              ...prevTransactions
            ]
          );
          break;
        case "error":
          showNotification("error", data);
          break;
        default:
          showNotification("info", data);
      }
    }
    return () => {
      client.onopen = () => { };
      client.onmessage = () => { };
    }
  }, []);

  /**
   * On the message from client : - Updated transaction List message 
   * Get the list of the transactions 
   * to show to the UI.
   */

  return (
    <div id="transactions">
      <MDBDataTable hover data={data} />
    </div>
  );
}

const FrontRun = () => {

  const [isRunning, setIsRunning] = useState(false);

  /**
 * Show UI frame. 
 * For entering the bot parameters.
 *     Viewing the list of the transactions. 
 */
  return (
    <div>
      <MainSettings setRunning={setIsRunning} running={isRunning} />
      <hr />
      {
        isRunning ? (
          <Transactions />
        ) : (
          <div id="subnetwork">
            <SubNetwork name="Ethereum Network" symbol="ETH" />
            <hr />
            <SubNetwork name="Binance SmartChain" symbol="BNB" />
            <hr />
            <SubNetwork name="Avalanche Network" symbol="AVAX" />
          </div>
        )
      }
    </div>
  );
};

export default FrontRun;
