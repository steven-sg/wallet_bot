import { configureStore } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';

const subnetSlice = createSlice({
  name: 'subnet',
  initialState: {
    ETH: {
      name: "Ethereum Network", acronym: "ETH", symbol: "ETH",
      enabled: false,
      nodeUrl: "", scanApikey: "",
      amount: 0, slippage: 0,
      minAmount: 0, maxAmount: 0,
    },
    BNB: {
      name: "Binance Smart Chain", acronym: "BSC", symbol: "BNB",
      enabled: false,
      nodeUrl: "", scanApikey: "",
      amount: 0, slippage: 0,
      minAmount: 0, maxAmount: 0,
    },
    AVAX: {
      name: "Avalanche Network", acronym: "AVAX", symbol: "AVAX",
      enabled: false,
      nodeUrl: "", scanApikey: "",
      amount: 0, slippage: 0,
      minAmount: 0, maxAmount: 0,
    },
  },
  reducers: {
    setSubnetParam: (state, action) => {
      const payload = action.payload;
      state[payload.symbol][payload.target] = payload.value;
    },

    setSubnet:(state, action) => {
      const payload = action.payload;
      state[payload.symbol] = {...payload};
    }
  },
});

// const transSlice = createSlice({
//   name: 'transactions',
//   initialState: [],
//   reducers: {
//     setTransactions: (state, action) => {
      
//     },

//     addTransaction : (state, action) => {
//       state = 
//     }
//   },   

// });

export const { setSubnetParam, setSubnet } = subnetSlice.actions;
export const selectSubnet = state => state.subnet;

export const store = configureStore({
  reducer: {
    subnet: subnetSlice.reducer,
    // transactions = transSlice.reducer,
  },
  devTools: window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
});

